import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Basse } from '../mocks/basses';
import { BASSES } from '../mocks/mock-basses';

@Component({
  selector: 'app-basse-detail',
  templateUrl: './basse-detail.component.html',
  styleUrls: ['../styles/styleDetail.css']
})
export class BasseDetailComponent implements OnInit {

  basses: Basse[] = null;
  basse: Basse = null;

  @ViewChild('modal') modal: ElementRef;

  toggleModal() {
    this.modal.nativeElement.classList.toggle('is-active');
  }

  closeModal() {
    this.modal.nativeElement.classList.remove('is-active');
  }

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.basses = BASSES;

    let id = +this.route.snapshot.paramMap.get('id');
    for (let i = 0; i < this.basses.length; i++) {
      if (this.basses[i].id == id) {
        this.basse = this.basses[i];
      }
    }
  }

}
