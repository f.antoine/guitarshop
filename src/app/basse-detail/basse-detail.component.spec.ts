import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasseDetailComponent } from './basse-detail.component';

describe('BasseDetailComponent', () => {
  let component: BasseDetailComponent;
  let fixture: ComponentFixture<BasseDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasseDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasseDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
