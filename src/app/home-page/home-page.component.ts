import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Ampli } from '../mocks/amplis';
import { APMLIS } from '../mocks/mock-amplis';

import { Guitare } from '../mocks/guitares';
import { GUITARES } from '../mocks/mock-guitares';

import { Basse } from '../mocks/basses';
import { BASSES } from '../mocks/mock-basses';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['../styles/styleOnly.css']
})
export class HomePageComponent implements OnInit {

  amplis: Ampli[] = null;
  guitares: Guitare[] = null;
  basses: Basse[] = null;

  constructor(private router: Router) { }

  ngOnInit() {
    this.amplis = APMLIS;
    this.guitares = GUITARES;
    this.basses = BASSES;
  }

  selectGuitare(guitare: Guitare): void {
		let link = ['/guitare', guitare.id];
		this.router.navigate(link);
  }
  
  selectBasse(basse: Basse): void {
		let link = ['/basse', basse.id];	
		this.router.navigate(link);
  }
  
  selectAmpli(ampli: Ampli): void {
		let link = ['/ampli', ampli.id];
		this.router.navigate(link);
	}

}
