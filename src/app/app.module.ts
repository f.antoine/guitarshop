import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { GuitarOnlyComponent } from './guitar-only/guitar-only.component';
import { GuitarDetailComponent } from './guitar-detail/guitar-detail.component';
import { BasseOnlyComponent } from './basse-only/basse-only.component';
import { BasseDetailComponent } from './basse-detail/basse-detail.component';
import { AmpliDetailComponent } from './ampli-detail/ampli-detail.component';
import { AmpliOnlyComponent } from './ampli-only/ampli-only.component';
import { ErrorPageComponent } from './error-page/error-page.component';
@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    GuitarOnlyComponent,
    GuitarDetailComponent,
    BasseOnlyComponent,
    BasseDetailComponent,
    AmpliDetailComponent,
    AmpliOnlyComponent,
    ErrorPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
