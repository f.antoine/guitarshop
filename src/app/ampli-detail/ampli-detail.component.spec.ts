import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmpliDetailComponent } from './ampli-detail.component';

describe('AmpliDetailComponent', () => {
  let component: AmpliDetailComponent;
  let fixture: ComponentFixture<AmpliDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmpliDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmpliDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
