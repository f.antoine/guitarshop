import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Ampli } from '../mocks/amplis';
import { APMLIS } from '../mocks/mock-amplis';

@Component({
  selector: 'app-ampli-detail',
  templateUrl: './ampli-detail.component.html',
  styleUrls: ['../styles/styleDetail.css']
})

export class AmpliDetailComponent implements OnInit {

  amplis: Ampli[] = null;
  ampli: Ampli = null;

  @ViewChild('modal') modal: ElementRef;

  toggleModal() {
    this.modal.nativeElement.classList.toggle('is-active');
  }

  closeModal() {
    this.modal.nativeElement.classList.remove('is-active');
  }

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.amplis = APMLIS;

    let id = +this.route.snapshot.paramMap.get('id');
    for (let i = 0; i < this.amplis.length; i++) {
      if (this.amplis[i].id == id) {
        this.ampli = this.amplis[i];
      }
    }
  }

}
