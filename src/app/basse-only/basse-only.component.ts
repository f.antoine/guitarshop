import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Basse } from '../mocks/basses';
import { BASSES } from '../mocks/mock-basses';

@Component({
  selector: 'app-basse-only',
  templateUrl: './basse-only.component.html',
  styleUrls: ['../styles/styleOnly.css']
})
export class BasseOnlyComponent implements OnInit {

  basses: Basse[] = null;

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.basses = BASSES;
  }

  selectBasse(basse: Basse): void {
    let link = ['/basse', basse.id];
    this.router.navigate(link);
  }

}
