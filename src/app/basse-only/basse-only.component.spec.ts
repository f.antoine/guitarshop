import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasseOnlyComponent } from './basse-only.component';

describe('BasseOnlyComponent', () => {
  let component: BasseOnlyComponent;
  let fixture: ComponentFixture<BasseOnlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasseOnlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasseOnlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
