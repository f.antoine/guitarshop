import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuitarOnlyComponent } from './guitar-only.component';

describe('GuitarOnlyComponent', () => {
  let component: GuitarOnlyComponent;
  let fixture: ComponentFixture<GuitarOnlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuitarOnlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuitarOnlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
