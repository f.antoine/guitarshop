import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Guitare } from '../mocks/guitares';
import { GUITARES } from '../mocks/mock-guitares';
@Component({
  selector: 'app-guitar-only',
  templateUrl: './guitar-only.component.html',
  styleUrls: ['../styles/styleOnly.css']
})
export class GuitarOnlyComponent implements OnInit {

  guitares: Guitare[] = null;

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.guitares = GUITARES;
  }

  selectGuitare(guitare: Guitare): void {
    let link = ['/guitare', guitare.id];
    this.router.navigate(link);
  }

}
