import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Guitare } from '../mocks/guitares';
import { GUITARES } from '../mocks/mock-guitares';

@Component({
  selector: 'app-guitar-detail',
  templateUrl: './guitar-detail.component.html',
  styleUrls: ['../styles/styleDetail.css']
})
export class GuitarDetailComponent implements OnInit {

  guitares: Guitare[] = null;
  guitare: Guitare = null;

  @ViewChild('modal') modal: ElementRef;

  toggleModal() {
    this.modal.nativeElement.classList.toggle('is-active');
  }

  closeModal() {
    this.modal.nativeElement.classList.remove('is-active');
  }

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.guitares = GUITARES;

    let id = +this.route.snapshot.paramMap.get('id');
    for (let i = 0; i < this.guitares.length; i++) {
      if (this.guitares[i].id == id) {
        this.guitare = this.guitares[i];
      }
    }
  }

}
