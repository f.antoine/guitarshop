import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ErrorPageComponent } from './error-page/error-page.component';

import { HomePageComponent } from './home-page/home-page.component';

import { GuitarOnlyComponent } from './guitar-only/guitar-only.component';
import { GuitarDetailComponent } from './guitar-detail/guitar-detail.component';

import { BasseOnlyComponent } from './basse-only/basse-only.component';
import { BasseDetailComponent } from './basse-detail/basse-detail.component';

import { AmpliOnlyComponent } from './ampli-only/ampli-only.component';
import { AmpliDetailComponent } from './ampli-detail/ampli-detail.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'guitare', component: GuitarOnlyComponent },
  { path: 'guitare/:id', component: GuitarDetailComponent },
  { path: 'basse', component: BasseOnlyComponent },
  { path: 'basse/:id', component: BasseDetailComponent },
  { path: 'ampli', component: AmpliOnlyComponent },
  { path: 'ampli/:id', component: AmpliDetailComponent },
  { path: '**', component: ErrorPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
