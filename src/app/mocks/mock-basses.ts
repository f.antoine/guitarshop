import { Basse } from './basses';

export const BASSES: Basse[]  = [
	{
        id: 1,
        marque: "Yamaha",
        prix: 250,
        img: "https://www.scottomusique.com/8396-large_default/guitare-basse-yamaha-bb-ne-2-nathan-east.jpg",
    },
    {
        id: 2,
        marque: "Ibanez",
        prix: 1200,
        img: "http://ibanezguitarsaz.com/images/products/GSR205MBK_LRG.png",
	},
]