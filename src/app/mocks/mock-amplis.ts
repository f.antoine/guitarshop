import { Ampli } from './amplis';

export const APMLIS: Ampli[]  = [
	{
        id: 1,
        marque: "Blackstar",
        prix: 100,
        img: "http://www.musicplus.be/cms/images/stories/virtuemart/product/mbl-idcore10-b.png",
    },
    {
        id: 2,
        marque: "Yamaha",
        prix: 300,
        img: "http://i0.wp.com/piano.ma/shop/wp-content/uploads/2018/01/yamaha-thr5a-amplificador-de-guitarra-acustica-piano.ma-maroc.png",
    },
    {
        id: 3,
        marque: "Fender",
        prix: 900,
        img: "http://i3.wp.com/piano.ma/shop/wp-content/uploads/2018/02/champion-20-fender-piano.ma-maroc-416x416.png",
	},
]