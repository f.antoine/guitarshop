import { Guitare } from './guitares';

export const GUITARES: Guitare[] = [
    {
        id: 1,
        marque: "Fender",
        prix: 800,
        img: "http://freepngimages.com/wp-content/uploads/2015/08/fender-stratocaster-sunburst.png",
    },
    {
        id: 2,
        marque: "Gibson",
        prix: 1500,
        img: "https://i1.wp.com/freepngimages.com/wp-content/uploads/2015/11/gibson-les-paul-tribute-p90-electric-guitar.png?fit=600%2C453",
    },
    {
        id: 3,
        marque: "Gibson",
        prix: 15000,
        img: "http://www.freepngclipart.com/save-png/44795-thoroughbred-dean-electric-guitar-brands,-seven-string-gibson/700x628",
    },
    {
        id: 4,
        marque: "Yamaha",
        prix: 500,
        img: "https://purepng.com/public/uploads/large/purepng.com-electric-guitarelectric-guitarsteelstringselectrical-1421526493452a0ddc.png",
    },
    {
        id: 5,
        marque: "Bohemian",
        prix: 300,
        img: "http://contestchest.com/images/bohemian-honey-oil-can-guitar.png",
    },
]