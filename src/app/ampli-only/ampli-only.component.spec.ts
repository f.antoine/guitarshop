import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmpliOnlyComponent } from './ampli-only.component';

describe('AmpliOnlyComponent', () => {
  let component: AmpliOnlyComponent;
  let fixture: ComponentFixture<AmpliOnlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmpliOnlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmpliOnlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
