import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Ampli } from '../mocks/amplis';
import { APMLIS } from '../mocks/mock-amplis';

@Component({
  selector: 'app-ampli-only',
  templateUrl: './ampli-only.component.html',
  styleUrls: ['../styles/styleOnly.css']
})
export class AmpliOnlyComponent implements OnInit {

  amplis: Ampli[] = null;

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.amplis = APMLIS;
  }

  selectAmpli(ampli: Ampli): void {
    let link = ['/ampli', ampli.id];
    this.router.navigate(link);
  }

}
